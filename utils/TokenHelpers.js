const jwt = require("jsonwebtoken");
const config = require("../config/default.json");
import { v4 as uuidv4 } from "uuid";
import connectDB from "../middleware/database.js";

import Token from "../models/Token";
import Reset from "../models/Reset";

const createTokenPair = async (accessPayload, refreshPayload) => {
  await connectDB();
  const date = Date.now();
  const accessExpired = 172800; // 48 часов - время жизни accessToken
  const expiredDate = date + accessExpired * 1000; // дата смерти accessToken
  const code = uuidv4();
  const accessToken = await jwt.sign(
    { ...accessPayload, uuid: code },
    config.jwtSecret,
    {
      expiresIn: accessExpired,
    }
  );
  const refreshToken = await jwt.sign(
    { ...refreshPayload, uuid: code },
    config.jwtRefreshSecret,
    {
      expiresIn: "30d",
    }
  );

  const RefreshToken = new Token({
    uuid: code,
    user: refreshPayload.user.id,
  });
  await RefreshToken.save();

  return {
    accessToken,
    refreshToken,
    expiredDate,
  };
};

const createResetToken = async (payload) => {
  const date = Date.now();
  const resetExpired = 3600000;
  const expiredDate = date + resetExpired * 1000;
  const code = uuidv4();
  const resetToken = await jwt.sign(
    { ...payload, uuid: code },
    config.jwtResetSecret,
    {
      expiresIn: expiredDate,
    }
  );
  // const query = {
  //   user: payload.user.id,
  //   resetToken: resetToken,
  // };

  const ResetToken = new Reset({
    user: payload.user.id,
    resetToken: resetToken,
  });

  await ResetToken.save();

  return {
    resetToken,
    expiredDate,
  };
};

module.exports = { createTokenPair, createResetToken };
