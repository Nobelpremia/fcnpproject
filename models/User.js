const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  subscribed: {
    type: Boolean,
    required: false,
  },
});

module.exports = mongoose.models?.User || mongoose.model("User", UserSchema);
