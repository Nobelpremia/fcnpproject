const mongoose = require("mongoose");

const RecordSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    registered: {
      type: Boolean,
      required: true,
    },
  },
  { versionKey: false }
);

module.exports =
  mongoose.models?.Record || mongoose.model("Record", RecordSchema);
