import mongoose from "mongoose";

const MONGO_URI =
  "mongodb://iMillUser1:712Bv1nnM@imillcluster1-shard-00-00-w7wjp.mongodb.net:27017,imillcluster1-shard-00-01-w7wjp.mongodb.net:27017,imillcluster1-shard-00-02-w7wjp.mongodb.net:27017/fcnpprojectDb?ssl=true&replicaSet=iMillCluster1-shard-0&authSource=admin&retryWrites=true&w=majority";

const connection = {};

async function connectDB() {
  if (connection.isConnected) {
    return;
  }
  const db = await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    serverSelectionTimeoutMS: 10000,
    useCreateIndex: true,
  });
  connection.isConnected = db.connections[0].readyState;
  console.log("Mongo Connected...");
}
export default connectDB;
