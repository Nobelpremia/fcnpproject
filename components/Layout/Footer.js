import React from "react";
import Link from "next/link";
import socialConfig from "../../socialConfig.json";

const Footer = () => {
  let currentYear = new Date().getFullYear();

  return (
    <footer className="footer-area">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-4 col-md-12">
            <p className="copyright">
              <i className="flaticon-copyright"></i> this.poweredBy(Closegamer).
              All Rights Reserved, @{currentYear}
            </p>
          </div>

          <div className="col-lg-4 col-md-12">
            <ul className="social">
              <li>
                <Link href={socialConfig.facebookLink}>
                  <a target="_blank">
                    <i className="flaticon-facebook-logo"></i>
                  </a>
                </Link>
              </li>
              {/* <li>
                <Link href="#">
                  <a target="_blank">
                    <i className="flaticon-twitter"></i>
                  </a>
                </Link>
              </li> */}
              <li>
                <Link href={socialConfig.instagramLink}>
                  <a target="_blank">
                    <i className="flaticon-instagram"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href={socialConfig.vkontakteLink}>
                  <a target="_blank">
                    <i className="flaticon-linkedin">VK</i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href={socialConfig.youTubeLink}>
                  <a target="_blank">
                    <i className="flaticon-youtube"></i>
                  </a>
                </Link>
              </li>
            </ul>
          </div>

          <div className="col-lg-4 col-md-12">
            {/* <ul className="info-link">
              <li>
                <Link href="#">
                  <a>Политика конфиденциальности</a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a>Пользовательское соглашение</a>
                </Link>
              </li>
            </ul> */}
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
