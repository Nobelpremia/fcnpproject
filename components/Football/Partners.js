import React from "react";
import Link from "next/link";

const Partners = () => {
  return (
    <section id="partners" className="partners-area bg-161616 pt-100 pb-70">
      <div className="container">
        <div className="section-title">
          <h2>Партнеры </h2>
          <p>
            Наши партнеры спонсируют деятельность школы, проводят конференции и
            трансферы на международном уровне. Иногда играют в футбол вместе с
            нами.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner1.jpg"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner2.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner3.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner4.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner5.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner6.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner7.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-4 col-md-4 col-6">
            <div className="single-partners-box">
              <Link href="#">
                <a>
                  <img
                    src="images/football/partner/footb-partner8.png"
                    alt="image"
                  />
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Partners;
