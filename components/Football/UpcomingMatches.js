import React, { Component } from "react";
import Link from "next/link";
import dynamic from "next/dynamic";
const OwlCarousel = dynamic(import("react-owl-carousel3"));

const options = {
  loop: true,
  nav: true,
  dots: false,
  autoplayHoverPause: true,
  autoplay: false,
  margin: 30,
  navText: [
    "<i class='flaticon-right-arrow'></i>",
    "<i class='flaticon-right-arrow'></i>",
  ],
  responsive: {
    0: {
      items: 1,
    },
    576: {
      items: 2,
    },
    768: {
      items: 2,
    },
    1200: {
      items: 2,
    },
  },
};

class UpcomingMatches extends Component {
  state = {
    display: false,
  };

  componentDidMount() {
    this.setState({ display: true });
  }

  render() {
    return (
      <section id="matches" className="upcoming-matches-area pt-100 pb-70">
        <div className="container">
          <div className="section-title">
            <h2>Предстоящие матчи воспитанников школы</h2>
            <p>
              Каждую неденю проводятся показательные матчи с участием
              воспитанников школы, чтобы каждый мог оценить приобретенное в
              школе мастерство маленьких игроков.
            </p>
          </div>

          {this.state.display ? (
            <OwlCarousel
              className="upcoming-matches-slides owl-carousel owl-theme"
              {...options}
            >
              <div className="single-upcoming-matches-item">
                <div className="date">
                  <span>15 Сентября, 2020</span>
                </div>
                <h3>Полуфинал</h3>
                <span className="sub-title">Лига чемпионов</span>

                <div className="vs-matches">
                  <img src="images/football/footb-team1.png" alt="image" />
                  <h4>ФК "Химки"</h4>
                  <span>VS</span>
                  <h4>ФШ "Новое поколение"</h4>
                  <img src="images/football/footb-team2.png" alt="image" />
                </div>

                <Link href="#">
                  <a className="default-btn">Купить билет</a>
                </Link>
              </div>

              <div className="single-upcoming-matches-item">
                <div className="date">
                  <span>22 Сентября, 2020</span>
                </div>
                <h3>Финал</h3>
                <span className="sub-title">Лига чемпионов</span>

                <div className="vs-matches">
                  <img src="images/football/footb-team1.png" alt="image" />
                  <h4>ФШ "Новое поколение"</h4>
                  <span>VS</span>
                  <h4>ФК "Зенит"</h4>
                  <img src="images/football/footb-team2.png" alt="image" />
                </div>

                <Link href="#">
                  <a className="default-btn">Купить билет</a>
                </Link>
              </div>

              <div className="single-upcoming-matches-item">
                <div className="date">
                  <span>31 декабря, 2020</span>
                </div>
                <h3>Первый тур</h3>
                <span className="sub-title">Чемпионат СНГ</span>

                <div className="vs-matches">
                  <img src="images/football/footb-team1.png" alt="image" />
                  <h4>ФШ "Новое поколение"</h4>
                  <span>VS</span>
                  <h4>ФК "Спартак"</h4>
                  <img src="images/football/footb-team2.png" alt="image" />
                </div>

                <Link href="#">
                  <a className="default-btn">Купить билет</a>
                </Link>
              </div>
            </OwlCarousel>
          ) : (
            ""
          )}
        </div>

        <div className="upcoming-matches-shape1">
          <img src="images/football/footb-player1.png" alt="image" />
        </div>
      </section>
    );
  }
}

export default UpcomingMatches;
