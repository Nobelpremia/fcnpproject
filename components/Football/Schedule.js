import React from "react";
import Link from "next/link";

const Schedule = () => {
  return (
    <section id="schedule" className="subscribe-area">
      <div className="container">
        <div className="subscribe-inner-area">
          <div className="section-title">
            <h2>Расписание</h2>

            <h3>Теплый стан</h3>
            <p>
              <i>Дети 4-8 лет</i>
            </p>
            <p>Понедельник, среда, пятница - 18:00 - 19:00</p>
            <h3>Медведково</h3>
            <p>Идет набор учеников.</p>
          </div>

          <div className="subscribe-shape1">
            <img src="images/football/football1.png" alt="image" />
          </div>
          <div className="subscribe-shape2">
            <img src="images/football/football2.png" alt="image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Schedule;
