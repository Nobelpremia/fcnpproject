import React, { useState } from "react";
import Link from "next/link";
import { useApp } from "../_App/appContext";

const Record = () => {
  let {
    state,
    handleSubmitRecord,
    changeMessageRecord,
    changePhoneRecord,
    changeEmailRecord,
    toState,
  } = useApp();

  // let isLoggedIn = toState.isLoggedIn;

  // let email = null;
  // if (toState.email) {
  //   email = toState.email;
  // }

  return (
    <section id="record" className="subscribe-area">
      <div className="container">
        <div className="subscribe-inner-area">
          <div className="section-title">
            <h2>Запись на занятия</h2>
            <p>
              Здесь можно записать ученика на занятия. <br />
            </p>
          </div>
          <form>
            <input
              className="input-newsletter"
              placeholder="Email"
              name="email"
              type="email"
              onChange={changeEmailRecord}
            />
            <br />
            <input
              className="input-newsletter"
              placeholder="Телефон"
              name="phone"
              type="digits"
              onChange={changePhoneRecord}
            />
            <br />
            <textarea
              className="input-newsletter"
              placeholder="Сообщение"
              name="message"
              type="text"
              maxLength="300"
              rows="100"
              onChange={changeMessageRecord}
            ></textarea>
            <br />
            <button type="button" onClick={handleSubmitRecord}>
              Записаться
            </button>
            <div id="errorRecordMonitor"></div>
          </form>
          <div className="subscribe-shape1">
            <img src="images/football/football1.png" alt="image" />
          </div>
          <div className="subscribe-shape2">
            <img src="images/football/football2.png" alt="image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Record;
