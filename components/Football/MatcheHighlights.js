import React, { Component } from "react";
import Link from "next/link";
import dynamic from "next/dynamic";
const OwlCarousel = dynamic(import("react-owl-carousel3"));
import ModalVideo from "react-modal-video";

const options = {
  loop: true,
  nav: true,
  dots: false,
  autoplayHoverPause: true,
  autoplay: false,
  items: 1,
  navText: ["<i class='flaticon-left'></i>", "<i class='flaticon-right'></i>"],
};

class MatcheHighlights extends Component {
  // Carousel
  state = {
    display: false,
  };

  componentDidMount() {
    this.setState({ display: true });
  }

  // Popup Video
  state = {
    isOpen: false,
  };
  openModal = () => {
    this.setState({ isOpen: true });
  };

  render() {
    return (
      <React.Fragment>
        <div id="highlights">
          {this.state.display ? (
            <OwlCarousel
              className="matches-highlights-slides owl-carousel owl-theme"
              {...options}
            >
              <div className="single-matches-highlights-item highlights-bg1">
                <div className="container">
                  <div className="row align-items-center">
                    <div className="col-lg-6 col-md-6">
                      <div className="content">
                        <h3>Тренировка</h3>
                        <span>14 апреля, 2020</span>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-6">
                      <div className="highlights-video">
                        <Link href="#play-video">
                          <a
                            onClick={(e) => {
                              e.preventDefault();
                              this.openModal();
                            }}
                            className="video-btn popup-youtube"
                          >
                            <span> </span>
                            <i className="flaticon-play-button"></i>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="single-matches-highlights-item highlights-bg2">
                <div className="container">
                  <div className="row align-items-center">
                    <div className="col-lg-6 col-md-6">
                      <div className="content">
                        <h3>Реальная игра</h3>
                        <span>10 сентября 2020</span>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-6">
                      <div className="highlights-video">
                        <Link href="#play-video">
                          <a
                            onClick={(e) => {
                              e.preventDefault();
                              this.openModal();
                            }}
                            className="video-btn popup-youtube"
                          >
                            <span> </span>
                            <i className="flaticon-play-button"></i>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="single-matches-highlights-item highlights-bg3">
                <div className="container">
                  <div className="row align-items-center">
                    <div className="col-lg-6 col-md-6">
                      <div className="content">
                        <h3>Получение призов</h3>
                        <span>16 октября 2020</span>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-6">
                      <div className="highlights-video">
                        <Link href="#play-video">
                          <a
                            onClick={(e) => {
                              e.preventDefault();
                              this.openModal();
                            }}
                            className="video-btn popup-youtube"
                          >
                            <span> </span>
                            <i className="flaticon-play-button"></i>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="single-matches-highlights-item highlights-bg4">
                <div className="container">
                  <div className="row align-items-center">
                    <div className="col-lg-6 col-md-6">
                      <div className="content">
                        <h3>Отбор на олимпийсие игры</h3>
                        <span>23 августа 2020</span>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-6">
                      <div className="highlights-video">
                        <Link href="#play-video">
                          <a
                            onClick={(e) => {
                              e.preventDefault();
                              this.openModal();
                            }}
                            className="video-btn popup-youtube"
                          >
                            <span> </span>
                            <i className="flaticon-play-button"></i>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </OwlCarousel>
          ) : (
            ""
          )}
        </div>

        {/* If you want to change the video need to update below videoID */}
        <ModalVideo
          channel="youtube"
          isOpen={this.state.isOpen}
          videoId="2Le9TVyWpLY"
          onClose={() => this.setState({ isOpen: false })}
        />
      </React.Fragment>
    );
  }
}

export default MatcheHighlights;
