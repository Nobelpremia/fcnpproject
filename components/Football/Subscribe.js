import React from "react";
import Link from "next/link";
import { useApp } from "../_App/appContext";

const Subscribe = () => {
  let { state, handleSubscribed, toState } = useApp();
  return (
    <section id="subscribe" className="subscribe-area">
      <div className="container">
        <div className="subscribe-inner-area">
          <div className="section-title">
            <h2>Подписка</h2>
            {toState.subscribed && (
              <p>
                Ваша подписка оформлена. <br />
                Теперь вы будете получать наши новости на указанный при
                регистрации Email.
              </p>
            )}
            {!toState.subscribed && (
              <React.Fragment>
                <p>
                  Подпишитесь на наши новости и будете всегда в курсе самых
                  свежих футбольных новостей, а также изменений расписания и тд.{" "}
                </p>
                <p>
                  Для оформления подписки надо зарегистрироваться или{" "}
                  <a href="#auth">войти в аккаунт</a>.
                </p>
              </React.Fragment>
            )}
          </div>
          {toState.subscribed && (
            <form className="newsletter-form">
              <button type="button" onClick={handleSubscribed}>
                Отменить подписку
              </button>
              <div id="errorSubscribedMonitor"></div>
            </form>
          )}
          {!toState.subscribed && toState.isLoggedIn && (
            <form className="newsletter-form">
              <button type="button" onClick={handleSubscribed}>
                Подписаться
              </button>
              <div id="errorSubscribedMonitor"></div>
            </form>
          )}
          {!toState.subscribed && !toState.isLoggedIn && (
            <form className="newsletter-form">
              {/* <button type="button" disabled>
                Подписаться
              </button> */}
              <div id="errorSubscribedMonitor"></div>
            </form>
          )}
          <div className="subscribe-shape1">
            <img src="images/football/football1.png" alt="image" />
          </div>
          <div className="subscribe-shape2">
            <img src="images/football/football2.png" alt="image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Subscribe;
