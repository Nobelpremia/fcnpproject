import React from "react";

const Trainer = () => {
  return (
    <section id="trainer" className="next-match-area">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="next-match-content">
              <div className="content">
                <div className="row align-items-left">
                  <div className="col-lg-12">
                    <h2>Тренер</h2>
                    <span className="sub-title">Курбатов Алексей Павлович</span>
                    <br />
                    <p>
                      Выпускник кафедры ТиМ Футбола Российского Государственного
                      Университета Физической Культуры Спорта и Туризма
                      (ГЦОЛИФК).
                    </p>
                    <p>
                      В 2017 г. прошел повеышение квалификации в "РГУФКСМиТ" и
                      получил тренерскую лицензию "С".
                    </p>
                    <p>Выпускник СДЮСШОР.</p>
                    <p>
                      Серебряный призер в первенстве России Московской области
                      по футболу среди юниоров.
                    </p>
                    <p>
                      Стаж работы в качестве тренера по футболу и преподавател
                      физической культуры с 2012 г..
                    </p>
                    <p>
                      В 2011 г. проходил обучение на курсах инструктора по
                      плаванию (РГУФКСМиТ).
                    </p>
                    <p>
                      В 2017 г. проходил обучение на курсах судей по футболу в
                      центре "Футбольный арбитр".
                    </p>
                  </div>
                </div>
              </div>

              <div className="shape1">
                <img src="images/football/footb-playing.png" alt="image" />
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="about-image">
              <img src="images/football/trainer.jpg" alt="image" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Trainer;
