import React from "react";
import Link from "next/link";
import socialConfig from "../../socialConfig.json";

const Banner = () => {
  return (
    <div id="home" className="main-banner jarallax">
      <div className="d-table">
        <div className="d-table-cell">
          <div className="container-fluid">
            <div className="main-banner-content">
              <span className="subtitle">Детская футбольная школа</span>
              <span className="title">Новое поколение</span> <br />
              <span className="title2">+7 (929) 626-69-64</span>
            </div>
          </div>
        </div>
      </div>

      <div className="banner-footer-content">
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-lg-6 col-sm-6">
              <span className="email-link">{socialConfig.mainEmail}</span>
            </div>

            <div className="col-lg-6 col-sm-6">
              <ul className="social">
                <li>
                  <Link href={socialConfig.facebookLink}>
                    <a target="_blank">
                      <i className="flaticon-facebook"></i>
                    </a>
                  </Link>
                </li>
                {/* <li>
                  <Link href="#">
                    <a target="_blank">
                      <i className="flaticon-twitter-1"></i>
                    </a>
                  </Link>
                </li> */}
                <li>
                  <Link href={socialConfig.instagramLink}>
                    <a target="_blank">
                      <i className="flaticon-instagram-1"></i>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href={socialConfig.vkontakteLink}>
                    <a target="_blank">
                      <i className="flaticon-vkontakte-1">
                        <img
                          className="vkLogo"
                          src="../images/football/vkIconWhite.png"
                        />
                      </i>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href={socialConfig.youTubeLink}>
                    <a target="_blank">
                      <i className="flaticon-youtube-1"></i>
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
