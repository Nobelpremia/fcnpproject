import React from "react";

const About = () => {
  return (
    <section id="about" className="next-match-area">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="about-image">
              <img src="images/football/footb-field2.jpg" alt="image" />
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="next-match-content">
              <div className="content">
                <div className="row align-items-left">
                  <div className="col-lg-12">
                    <h2>О нас</h2>
                    <span className="sub-title">"ДФШ Новое Поколение"</span>
                    <br />
                    <p>
                      Школа основана 3 сентября 2017 г. в г.Москва. Первое
                      отделение открылось в СВАО (ст. м. Медведково). Второе
                      отделение открылось 15 января 2018 г. в ЮЗАО (ст. м.
                      Теплый стан).
                    </p>
                    <p>
                      Участник и призер многочисленных соревнований среди детей
                      5-6 лет.
                    </p>
                    <p>
                      Основной принцип работы - это воспитание личности,
                      развитие индивидуальных технических и технико-тактических
                      навыков и умений.
                    </p>
                    <p>
                      Работа с футбольными тренажерами "Jimmi ball", "Junior
                      ball" для обучения технике удара, остановки и
                      жонглирования мяча.
                    </p>
                  </div>
                </div>
              </div>

              <div className="shape1">
                <img src="images/football/footb-playing.png" alt="image" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
