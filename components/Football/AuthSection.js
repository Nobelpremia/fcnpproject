import React from "react";
import { useApp } from "../_App/appContext";

const AuthSection = () => {
  let {
    state,
    handleSubmit,
    handleLogout,
    handleRecovery,
    changeUserName,
    changeEmail,
    changePassword,
    changePassword2,
    formMode,
    changeForm,
    toState,
    handleSubscribed,
    password,
    password2,
  } = useApp();

  let isLoggedIn = toState.isLoggedIn;
  let userName = toState.userName;
  let email = toState.email;

  return (
    <React.Fragment>
      <section id="auth" className="subscribe-area">
        {formMode == "login" && (
          <div className="container">
            <div className="subscribe-inner-area">
              <div className="section-title">
                <h2>Аккаунт</h2>
                {!isLoggedIn && (
                  <p>Войдите в аккаунт, чтобы получать больше информации.</p>
                )}
              </div>
              <form>
                {!isLoggedIn && (
                  <React.Fragment>
                    <input
                      className="input-newsletter"
                      placeholder="Email"
                      name="email"
                      type="email"
                      onChange={changeEmail}
                    />
                    <br />
                    <input
                      className="input-newsletter"
                      placeholder="Пароль"
                      name="password"
                      type="password"
                      onChange={changePassword}
                    />
                    <br />
                  </React.Fragment>
                )}
                {isLoggedIn && (
                  <p>
                    Вы вошли как {userName}. Ваш Email - {email}.
                  </p>
                )}
                {!isLoggedIn && (
                  <button type="button" onClick={handleSubmit}>
                    Войти
                  </button>
                )}
                {isLoggedIn && (
                  <button type="button" onClick={handleLogout}>
                    Выйти
                  </button>
                )}
                {!isLoggedIn && (
                  <React.Fragment>
                    <p>
                      Не зарегистрированы?{" "}
                      <a href="#auth" onClick={(e) => changeForm("register")}>
                        Регистрация
                      </a>
                      <span>&nbsp;|&nbsp;</span>
                      <a href="#auth" onClick={(e) => changeForm("recovery")}>
                        Сбросить пароль
                      </a>
                    </p>
                  </React.Fragment>
                )}
                <div id="errorMonitor"></div>
              </form>
            </div>
          </div>
        )}
        {formMode == "register" && (
          <div className="container">
            <div className="subscribe-inner-area">
              <div className="section-title">
                <h2>Регистрация</h2>
                {!isLoggedIn && (
                  <p>
                    Зарегистрируйтесь в системе, чтобы получать больше
                    информации.
                  </p>
                )}
              </div>
              <form>
                <React.Fragment>
                  <input
                    className="input-newsletter"
                    placeholder="Имя"
                    name="userName"
                    type="text"
                    onChange={changeUserName}
                  />
                  <br />
                  <input
                    className="input-newsletter"
                    placeholder="Email"
                    name="email"
                    type="email"
                    onChange={changeEmail}
                  />
                  <br />
                  <input
                    className="input-newsletter"
                    placeholder="Пароль"
                    password="password"
                    type="password"
                    onChange={changePassword}
                  />
                  <br />
                  <input
                    className="input-newsletter"
                    placeholder="Подтверждение пароля"
                    password="password2"
                    type="password"
                    onChange={changePassword2}
                  />
                  <br />
                </React.Fragment>

                <button type="button" onClick={handleSubmit}>
                  Зарегистрироваться
                </button>

                {!isLoggedIn && (
                  <p>
                    Уже есть аккаунт?{" "}
                    <a href="#auth" onClick={(e) => changeForm("login")}>
                      Войти
                    </a>
                  </p>
                )}
                <div id="errorMonitor"></div>
              </form>
            </div>
          </div>
        )}
        {formMode == "recovery" && (
          <div className="container">
            <div className="subscribe-inner-area">
              <div className="section-title">
                <h2>Сброс пароля</h2>
                {!isLoggedIn && (
                  <p>
                    Введите Ваш email и мы вышлем на него ссылку для сброса
                    пароля.
                  </p>
                )}
              </div>
              <form>
                <React.Fragment>
                  <input
                    className="input-newsletter"
                    placeholder="Email"
                    name="email"
                    type="email"
                    onChange={changeEmail}
                  />
                  <br />
                </React.Fragment>

                <button type="button" onClick={handleRecovery}>
                  Сбросить пароль
                </button>

                {!isLoggedIn && (
                  <p>
                    Вспомнили пароль?{" "}
                    <a href="#auth" onClick={(e) => changeForm("login")}>
                      Войти
                    </a>
                  </p>
                )}
                <div id="errorMonitor"></div>
              </form>
            </div>
          </div>
        )}
      </section>
    </React.Fragment>
  );
};

export default AuthSection;
