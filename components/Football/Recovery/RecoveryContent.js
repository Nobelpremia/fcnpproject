import React, { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";

const RecoveryContent = (props) => {
  const router = useRouter();
  const { email, resetToken } = props;
  console.log("email in recovery content", email);

  if (!email) return <p>Ожидайте загрузки данных пользователя...</p>;
  if (!resetToken) return <p>Ожидайте загрузки токена...</p>;

  let initialState = {
    email,
    password: "",
    password2: "",
    resetToken,
  };

  let [state, setState] = useState(initialState);

  const changePassword = (e) => {
    let password = e.target.value;
    setState({ ...state, password });
  };

  const changePassword2 = (e) => {
    let password2 = e.target.value;
    setState({ ...state, password2 });
  };

  const handlePasswordChange = async () => {
    document.getElementById("errorResetMonitor").innerHTML = "";
    try {
      const res = await axios.post("/api/setPassword", state);
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        document.getElementById("errorResetMonitor").innerHTML =
          "Пароль успешно изменен и выслан на Вашу почту! <br> Скоро Вы будете перенаправлены на главную страницу.";

        const res2 = await axios.post("/api/deleteResetToken", {
          resetToken: state.resetToken,
        });

        router.push("/#auth");
      }
    } catch (error) {
      document.getElementById("errorResetMonitor").innerHTML = error;
    }
  };

  return (
    <div id="subscribe" className="subscribe-area">
      <div className="container">
        <div className="subscribe-inner-area">
          <div className="section-title">
            <h2>Новый пароль</h2>
            {email && (
              <React.Fragment>
                <p>
                  Ваш email - {email}. <br />
                </p>
                <p>
                  Здесь Вы можете задать новый пароль. <br />
                </p>
              </React.Fragment>
            )}
            {!email && (
              <React.Fragment>
                <p>
                  Пользователь не определен. Сброс пароля невозможен.
                  <br />
                </p>
              </React.Fragment>
            )}
          </div>

          <form className="newsletter-form">
            <input
              className="input-newsletter"
              placeholder="Новый пароль"
              name="password"
              type="password"
              onChange={changePassword}
            />
            <br />
            <input
              className="input-newsletter"
              placeholder="Подтверждение пароля"
              name="password2"
              type="password"
              onChange={changePassword2}
            />
            <br />
            <button type="button" onClick={handlePasswordChange}>
              Сохранить
            </button>
            <div id="errorResetMonitor"></div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RecoveryContent;
