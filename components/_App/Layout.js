import React, { useState } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Navbar from "../Layout/Navbar";
import Footer from "../Layout/Footer";
import GoTop from "../Shared/GoTop";

const Layout = ({ children }) => {
  const router = useRouter();
  return (
    <React.Fragment>
      <Head>
        <title>ДФШ Новое поколение</title>
        <meta
          name="description"
          content="Официальный сайт Детской Футбольной Школы Новое поколение. Расписание, запись на занятия, информаия о школе и тренерах, фотографии процесса обучения."
        />
        <meta
          name="keywords"
          content="футбол, школа, Курбатов, Алексей, медведково, теплый, стан, дети, футболист, карьера."
        />
        <meta
          name="og:title"
          property="og:title"
          content="ДФШ Новое поколение"
        ></meta>
        <meta name="twitter:card" content="ДФШ Новое поколение"></meta>
        <link rel="canonical" href="https://fcnew-pokolenie.ru/"></link>
      </Head>
      {router.pathname === "/recovery/[token]" ? null : <Navbar />}
      {children}
      <Footer />
      <GoTop scrollStepInPx="100" delayInMs="10.50" />
    </React.Fragment>
  );
};

export default Layout;
