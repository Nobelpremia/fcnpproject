import React, { useState, createContext, useContext } from "react";
import axios from "axios";
import { setCookies, getCookies, removeCookies } from "cookies-next";
import useSWR from "swr";

const AppContext = createContext();

let initialState = {
  isLoggedIn: false,
  userName: "",
  email: "",
  role: "",
  password: "",
  password2: "",
  formMode: "login",
  subscribed: false,
  message: "",
  phone: "",
};

export const useApp = () => {
  return useContext(AppContext);
};

const getUser = async () => {
  const accessToken = getCookies(null, "accessToken");
  const refreshToken = getCookies(null, "refreshToken");
  if (!accessToken && !refreshToken) {
    return;
  }
  let response = await axios.post("/api/checkIn", {
    accessToken,
    refreshToken,
  });
  return response;
};

export const Provider = ({ children }) => {
  let user = null;
  let tokens = null;

  const { data: userData } = useSWR("/api/checkIn", getUser);

  if (userData) {
    user = userData.data.user;
    if (userData.data.tokens && userData.data.tokens !== "prolongated") {
      if (userData.data.tokens && userData.data.tokens !== "killRefresh") {
        if (
          userData.data.tokens.accessToken &&
          userData.data.tokens.refreshToken
        ) {
          removeCookies(null, "refreshToken");
          removeCookies(null, "accessToken");
          let new_minut = new Date();
          new_minut.setMinutes(2 + new_minut.getMinutes());

          setCookies(null, "accessToken", userData.data.tokens.accessToken, {
            expires: new_minut,
          });
          setCookies(null, "refreshToken", userData.data.tokens.refreshToken, {
            expires: 7,
          });
        } else {
          document.getElementById("errorMonitor").innerHTML =
            "токены не получились";
        }
      } else if (
        userData.data.tokens &&
        userData.data.tokens === "killRefresh"
      ) {
        removeCookies(null, "refreshToken");
      }
    }
  } else {
    console.log("checkIn - nothing to change");
  }

  let toState = initialState;
  if (user) {
    let newState = {
      isLoggedIn: true,
      userName: user.userName,
      email: user.email,
      role: user.role,
      password: "",
      password2: "",
      formMode: "login",
      subscribed: user.subscribed,
    };

    toState = newState;
  }

  let [state, setState] = useState(toState);

  const handleSubscribed = async (values) => {
    let subscribedStatus = toState.subscribed;
    if (subscribedStatus == true) {
      subscribedStatus = false;
    } else if (subscribedStatus == false) {
      subscribedStatus = true;
    }
    let email = toState.email;
    try {
      const res = await axios.post("/api/subscribed", {
        subscribedStatus,
        email,
      });
      if (!res.data.success) {
        if (res.data.error) {
          console.log("res from subscribe", res);
          throw res.data.error;
        }
      } else {
        const subscribed = res.data.subscribed;
        setState({ ...state, subscribed });
        document.getElementById("errorSubscribedMonitor").innerHTML = "Успешно";
      }
    } catch (error) {
      document.getElementById("errorSubscribedMonitor").innerHTML = error;
    }
  };

  const handleSubmit = async () => {
    try {
      const res = await axios.post("/api/users", state);
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        const tokens = res.data.tokens;

        let new_minut = new Date();
        new_minut.setMinutes(2 + new_minut.getMinutes());
        setCookies(null, "accessToken", tokens.accessToken, {
          expires: new_minut,
        });
        setCookies(null, "refreshToken", tokens.refreshToken, { expires: 7 });

        const isLoggedIn = true;
        const userName = res.data.user.userName;
        const email = res.data.user.email;
        const role = res.data.user.role;
        const formMode = "login";
        setState({ ...state, isLoggedIn, email, userName, role, formMode });

        document.getElementById("errorMonitor").innerHTML = "Успешный вход";
      }
    } catch (error) {
      document.getElementById("errorMonitor").innerHTML = error;
    }
  };

  const handleLogout = async () => {
    const accessCookieValue = getCookies(null, "accessToken");
    try {
      const res = await axios.post("/api/logout", {
        accessToken: accessCookieValue,
      });
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        removeCookies(null, "accessToken");
        removeCookies(null, "refreshToken");
        const isLoggedIn = false;
        const userName = "";
        const email = "";
        const role = "";
        setState({ ...state, isLoggedIn, email, userName, role });

        document.getElementById("errorMonitor").innerHTML = "Успешный выход";
      }
    } catch (error) {
      document.getElementById("errorMonitor").innerHTML = error;
    }
  };

  const handleRecovery = async () => {
    try {
      console.log("handleRecovery state", state);
      const res = await axios.post("/api/recovery", state);
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        document.getElementById("errorMonitor").innerHTML =
          "Ссылка для сброса пароля отправлена на указанную почту";
      }
    } catch (error) {
      document.getElementById("errorMonitor").innerHTML = error;
    }
  };

  const changeUserName = (e) => {
    let userName = e.target.value;
    setState({ ...state, userName });
  };

  const changeEmail = (e) => {
    let email = e.target.value;
    setState({ ...state, email });
  };

  const changePassword = (e) => {
    let password = e.target.value;
    setState({ ...state, password });
  };

  const changePassword2 = (e) => {
    let password2 = e.target.value;
    setState({ ...state, password2 });
  };

  const changeForm = (formMode) => {
    setState({ ...state, formMode });
  };

  const handleSubmitRecord = async (values) => {
    try {
      const res = await axios.post("/api/record", state);
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        document.getElementById("errorRecordMonitor").innerHTML =
          "Ваша заявка отправлена. <br /> В ближайшее время с мы с Вами свяжемся";
      }
    } catch (error) {
      document.getElementById("errorRecordMonitor").innerHTML = error;
    }
  };

  const changeMessageRecord = (e) => {
    let message = e.target.value;
    setState({ ...state, message });
  };

  const changePhoneRecord = (e) => {
    let phone = e.target.value;
    setState({ ...state, phone });
  };

  const changeEmailRecord = (e) => {
    let email = null;
    if (e.target) {
      email = e.target.value;
    } else {
      email = e;
    }
    setState({ ...state, email });
  };

  return (
    <AppContext.Provider
      value={{
        ...state,
        handleSubmit,
        handleLogout,
        handleRecovery,
        changeUserName,
        changeEmail,
        changePassword,
        changePassword2,
        changeForm,
        handleSubscribed,
        handleSubmitRecord,
        changeMessageRecord,
        changePhoneRecord,
        changeEmailRecord,
        toState,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
