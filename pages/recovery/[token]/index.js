import React, { useState } from "react";
import RecoveryContent from "../../../components/Football/Recovery/RecoveryContent";
import { useRouter } from "next/router";
import axios from "axios";
const config = require("../../../config/default.json");

let initialState = {
  isLoggedIn: false,
  userName: "",
  email: "",
  resetToken: "",
};

const RecoveryPage = () => {
  const router = useRouter();
  let [state, setState] = useState(initialState);
  const { token } = router.query;
  let user = null;

  const handleReset = async (token) => {
    console.log("token inside [token]", token);
    try {
      const res = await axios.post(config.host + "api/reset", {
        token,
      });
      console.log("res inside [token]", res);
      if (!res.data.success) {
        if (res.data.error) {
          throw res.data.error;
        }
      } else {
        user = res.data.user;
        const isLoggedIn = true;
        const userName = res.data.user.userName;
        const email = res.data.user.email;
        const resetToken = token;
        setState({ ...state, isLoggedIn, email, userName, resetToken });
      }
    } catch (error) {
      res.json({
        success: false,
        error: "Сброс пароля не получился!",
      });
    }
  };

  if (token && state.email == "") {
    handleReset(token);
  }

  return (
    <React.Fragment>
      <RecoveryContent email={state.email} resetToken={state.resetToken} />
    </React.Fragment>
  );
};

export default RecoveryPage;
