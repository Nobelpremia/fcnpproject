import nextConnect from "next-connect";
import User from "../../models/User";
import connectDB from "../../middleware/database.js";
import axios from "axios";
const { createResetToken } = require("../../utils/TokenHelpers");
const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { email, formMode } = req.body;
  try {
    await connectDB();
    const user = await User.findOne({ email });
    if (formMode !== "recovery") {
      res.json({
        success: false,
        error: "Неверная операция!",
      });
    }

    if (!user) {
      res.json({
        success: false,
        error: "Пользователь не найден!",
      });
    }

    const payload = {
      user: {
        id: user._id,
      },
    };

    const tokenData = await createResetToken(payload);

    const recoveryLink =
      `<a href=` +
      config.host +
      `recovery/` +
      tokenData.resetToken +
      ">Сбросить пароль</a>";

    // отправка почты
    const adminMail = config.mailUser;
    const customerMail = email;

    const site = 'ДФШ "Новое Поколение"';

    const operation = "recovery";

    var adminSubjectPre = "Восстановление пароля";
    var customerSubjectPre = "Восстановление пароля";

    var adminMessagePre =
      "Восстановление пароля на сайте. Email:" + customerMail + ".";
    var customerMessagePre =
      "Для сброса пароля пройдите по ссылке: " + recoveryLink + ".";

    const response = await axios.get("http://vercelmail.com/api/mail", {
      params: {
        admin: adminMail,
        customer: customerMail,
        operation: operation,
        site: site,
        adminSubject: adminSubjectPre,
        customerSubject: customerSubjectPre,
        adminMessage: adminMessagePre,
        customerMessage: customerMessagePre,
      },
    });

    res.json({
      success: true,
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

export default handler;
