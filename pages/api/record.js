import nextConnect from "next-connect";
import connectDB from "../../middleware/database.js";
import User from "../../models/User";
import Record from "../../models/Record";
import axios from "axios";
const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { email, message, phone } = req.body;
  try {
    await connectDB();
    let registered = false;
    let record = {};

    let user = await User.findOne({ email });
    if (user) {
      registered = true;
    }
    record = new Record({
      email,
      message,
      phone,
      registered,
    });

    await record.save();

    // отправка почты
    const adminMail = config.mailUser;
    const customerMail = email;

    const site = 'ДФШ "Новое Поколение"';

    const operation = "record";

    var adminSubjectPre = "Новая запись ученика";
    var customerSubjectPre = "Успешная запись ученика";

    var registeredSign = "нет";
    if (registered == true) {
      registeredSign = "да";
    }
    var adminMessagePre =
      "Запись ученика. Email:" +
      customerMail +
      ", телефон: " +
      phone +
      ", сообщение: " +
      message +
      ", зарегистрирован на сайте: " +
      registeredSign +
      ".";

    var customerMessagePre =
      "Спасибо! Вы оставили сообщение о записи ученика. Мы с Вами свяжемся.";

    const response = await axios.get("http://vercelmail.com/api/mail", {
      params: {
        admin: adminMail,
        customer: customerMail,
        operation: operation,
        site: site,
        adminSubject: adminSubjectPre,
        customerSubject: customerSubjectPre,
        adminMessage: adminMessagePre,
        customerMessage: customerMessagePre,
      },
    });
    res.json({ success: true });
  } catch (error) {
    res.json({ success: false, error });
  }
});

export default handler;
