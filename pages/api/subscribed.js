import nextConnect from "next-connect";
import User from "../../models/User";
import connectDB from "../../middleware/database.js";
import axios from "axios";
const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { subscribedStatus, email } = req.body;
  const subscribed = subscribedStatus;
  const adminMail = config.mailUser;
  const customerMail = email;

  const site = 'ДФШ "Новое Поколение"';

  const operation = "subscribe";

  var adminSubjectPre = "Новая подписка";
  var customerSubjectPre = "Успешная подписка";

  var adminMessagePre =
    "Кто-то подписался на новости Вашего сайта. Email:" + customerMail + ".";
  var customerMessagePre =
    "Поздравляем! Вы подписались на новости нашего сайта. Отменить подписку Вы сможете также на сайте.";

  if (subscribed == false) {
    adminSubjectPre = "Отмена подписки";
    customerSubjectPre = "Отмена подписки";
    adminMessagePre =
      "Пользователь отменил подписку на новости сайта. Email:" +
      customerMail +
      ".";
    customerMessagePre =
      "Вы отменили подписку на новости нашего сайта. Возобновить подписку снова Вы сможете на сайте.";
  }

  try {
    await connectDB();

    await User.findOneAndUpdate({ email }, { subscribed }, {});

    const response = await axios.get("http://vercelmail.com/api/mail", {
      params: {
        admin: adminMail,
        customer: customerMail,
        operation: operation,
        site: site,
        adminSubject: adminSubjectPre,
        customerSubject: customerSubjectPre,
        adminMessage: adminMessagePre,
        customerMessage: customerMessagePre,
      },
    });
    if (response.data.success == true) {
      res.json({ success: true });
    } else {
      res.json({ success: false });
    }
  } catch (error) {
    console.log(error);
    res.json({ success: false, error });
  }
});

export default handler;
