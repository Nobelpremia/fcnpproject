import nextConnect from "next-connect";
import User from "../../models/User";
import Reset from "../../models/Reset";
import connectDB from "../../middleware/database.js";
import jwt from "jsonwebtoken";
const config = require("../../config/default.json");
const handler = nextConnect();

handler.post(async (req, res) => {
  const { token } = req.body;
  if (token) {
    try {
      await connectDB();
      let resetDecoded = jwt.verify(token, config.jwtResetSecret);
      let date = Date.now();
      let resetExpired = resetDecoded.exp * 1000;

      if (date > resetExpired) {
        resetDecoded = null;
      }

      if (resetDecoded !== null) {
        const userFromResetToken = resetDecoded.user.id;
        const checkResetToken = await Reset.findOne({
          resetToken: token,
        });
        let user = null;
        let userId = null;

        if (checkResetToken) {
          userId = userFromResetToken;
          const userPre = await User.findOne({ _id: userId });
          if (!userPre) {
            res.json({
              success: false,
              error: "Пользователь не найден!",
            });
          }
          user = userPre;
          return res.json({ success: true, user });
        }
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
});

export default handler;
