import nextConnect from "next-connect";
import User from "../../models/User";
import connectDB from "../../middleware/database.js";
import axios from "axios";
const config = require("../../config/default.json");
const bcrypt = require("bcryptjs");
const handler = nextConnect();

handler.post(async (req, res) => {
  const { email, password, password2 } = req.body;

  try {
    await connectDB();

    if (password !== password2) {
      res.json({
        success: false,
        error: "Пароли не совпадают!",
      });
    }

    const user = await User.findOne({ email });

    if (!user) {
      res.json({
        success: false,
        error: "Пользователь не найден!",
      });
    }

    const salt = await bcrypt.genSalt(10);
    const passForMail = password;

    const newPassword = await bcrypt.hash(password, salt);

    await User.findOneAndUpdate({ email }, { password: newPassword }, {});

    // отправка почты
    const adminMail = config.mailUser;
    const customerMail = email;

    const site = 'ДФШ "Новое Поколение"';

    const operation = "setPassword";

    var adminSubjectPre = "Новый пароль";
    var customerSubjectPre = "Ваш новый пароль";

    var adminMessagePre = "Новый пароль на сайте. Email:" + customerMail + ".";
    var customerMessagePre = "Ваш новый пароль : " + passForMail + ".";

    const response = await axios.get("http://vercelmail.com/api/mail", {
      params: {
        admin: adminMail,
        customer: customerMail,
        operation: operation,
        site: site,
        adminSubject: adminSubjectPre,
        customerSubject: customerSubjectPre,
        adminMessage: adminMessagePre,
        customerMessage: customerMessagePre,
      },
    });

    res.json({ success: true });
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

export default handler;
