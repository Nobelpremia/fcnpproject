import nextConnect from "next-connect";
import connectDB from "../../middleware/database.js";
import Token from "../../models/Token";
import jwt from "jsonwebtoken";
const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { accessToken } = req.body;
  try {
    await connectDB();
    let accessTokenDecoded = jwt.verify(accessToken, config.jwtSecret);
    let accessUuid = accessTokenDecoded.uuid;
    await Token.deleteOne({ uuid: accessUuid });

    res.json({ success: true });
  } catch (error) {
    res.json({ success: false, error });
  }
});

export default handler;
