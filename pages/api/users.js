import nextConnect from "next-connect";
import User from "../../models/User";
import connectDB from "../../middleware/database.js";
import axios from "axios";
const { createTokenPair } = require("../../utils/TokenHelpers");
const bcrypt = require("bcryptjs");
const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { email, userName, password, formMode } = req.body;
  if (formMode == "login") {
    try {
      await connectDB();
      let user = await User.findOne({ email });
      if (!user) {
        res.json({
          success: false,
          error: "Пользователь не найден!",
        });
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        res.json({
          success: false,
          error: "Email или пароль введены неверно!",
        });
      }
      const payload = {
        user: {
          id: user.id,
        },
      };
      const tokensData = await createTokenPair(payload, payload);
      res.json({ success: true, user, tokens: tokensData });
    } catch (error) {
      res.json({ success: false, error });
    }
  } else if (formMode == "register") {
    try {
      await connectDB();
      let user = await User.findOne({ email });
      if (user) {
        res.json({
          success: false,
          error: "Пользователь уже зарегистрирован!",
        });
      }

      let role = "customer";

      if (email == config.mailUser) {
        role = "admin";
      }

      let subscribed = false;

      user = new User({
        userName,
        email,
        password,
        role,
        subscribed,
      });

      const salt = await bcrypt.genSalt(10);

      const passForMail = password;

      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      const tokensData = await createTokenPair(payload, payload);

      // отправка почты

      const adminMail = config.mailUser;
      const customerMail = email;

      const site = 'ДФШ "Новое Поколение"';

      const operation = "register";

      var adminSubjectPre = "Новая регистрация";
      var customerSubjectPre = "Успешная регистрация";

      var adminMessagePre =
        "Новая регистрация на сайте. Username: " +
        userName +
        ", Email:" +
        customerMail +
        ".";
      var customerMessagePre =
        "Поздравляем! Вы успешно зарегистрировались на нашем сайте. Ваш пароль: " +
        passForMail +
        ".";

      const response = await axios.get("http://vercelmail.com/api/mail", {
        params: {
          admin: adminMail,
          customer: customerMail,
          operation: operation,
          site: site,
          adminSubject: adminSubjectPre,
          customerSubject: customerSubjectPre,
          adminMessage: adminMessagePre,
          customerMessage: customerMessagePre,
        },
      });

      res.json({ success: true, user, tokens: tokensData });
    } catch (error) {
      res.json({ success: false, error });
    }
  }
});

export default handler;
