import nextConnect from "next-connect";
import connectDB from "../../middleware/database.js";
import Token from "../../models/Token";
import User from "../../models/User";
import jwt from "jsonwebtoken";
const { createTokenPair } = require("../../utils/TokenHelpers");

const config = require("../../config/default.json");

const handler = nextConnect();

handler.post(async (req, res) => {
  const { accessToken, refreshToken } = req.body;
  try {
    await connectDB();
    if (accessToken) {
      let accessTokenDecoded = jwt.verify(accessToken, config.jwtSecret);

      let date = Date.now();
      let accessExpired = accessTokenDecoded.exp * 1000;
      let accessDecodedUuid = accessTokenDecoded.uuid;

      if (date > accessExpired) {
        accessTokenDecoded = null;
      }

      if (accessTokenDecoded !== null) {
        const userFromAccessToken = accessTokenDecoded.user.id;

        const checkAccessToken = await Token.findOne({
          uuid: accessDecodedUuid,
        });
        let user = null;
        let userId = null;

        if (checkAccessToken) {
          userId = userFromAccessToken;
          const userPre = await User.findOne({ _id: userId });
          if (!userPre) {
            res.json({
              success: false,
              error: "Пользователь не найден!",
            });
          }
          user = userPre;
          const tokensData = "prolongated";
          res.json({ success: true, user, tokens: tokensData });
        }
      }
    }

    if (!accessToken || accessToken == undefined || accessToken == null) {
      const refreshTokenDecoded = jwt.verify(
        refreshToken,
        config.jwtRefreshSecret
      );
      let refreshDecodedUuid = refreshTokenDecoded.uuid;
      let refreshExpired = refreshTokenDecoded.exp * 1000;
      let date = Date.now();
      if (date > refreshExpired) {
        refreshTokenDecoded = null;
      }
      if (refreshTokenDecoded !== null) {
        const userFromRefreshToken = refreshTokenDecoded.user.id;
        const checkRefreshToken = await Token.findOne({
          uuid: refreshDecodedUuid,
        });
        if (checkRefreshToken) {
          let user = null;
          let userId = userFromRefreshToken;

          user = await User.findOne({ _id: userId });
          if (!user) {
            res.json({
              success: false,
              error: "Пользователь не найден!",
            });
          }

          const payload = {
            user: {
              id: user.id,
            },
          };

          await Token.deleteOne({ uuid: refreshDecodedUuid });

          const tokensData = await createTokenPair(payload, payload);
          res.json({ success: true, user, tokens: tokensData });
        } else {
          res.json({ success: true, tokens: "killRefresh" });
        }
      }
    }
  } catch (error) {
    res.json({ success: false, error });
  }
});

export default handler;
