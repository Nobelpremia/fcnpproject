import nextConnect from "next-connect";
import connectDB from "../../middleware/database.js";
import Reset from "../../models/Reset";

const handler = nextConnect();

handler.post(async (req, res) => {
  const { resetToken } = req.body;
  try {
    await connectDB();
    await Reset.deleteOne({ resetToken });

    res.json({ success: true });
  } catch (error) {
    res.json({ success: false, error });
  }
});

export default handler;
